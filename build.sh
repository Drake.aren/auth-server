#!/bin/sh

RELEASE_NAME=auth-server
WORK_DIR=$(pwd)

cd src
go build -o $RELEASE_NAME
mv $RELEASE_NAME $WORK_DIR/dist
cd -