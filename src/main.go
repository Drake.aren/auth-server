package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
	"encoding/json"
)

func main() {

	router := NewRouter()

	log.Fatal(http.ListenAndServe(":4000", router))

}

type User struct {
	Login    string `json:"login"`
	Password string `json:"pass"`
	CreatedAt time.Time `json:"time"`
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Welcome to auth server!")

	user := User{
		Login: "Tomas",
		Password: "tomas123",
	}

	json.NewEncoder(w).Encode(user)
}

func Login(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Ready to login a user!")
}

func Create(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Ready to create a user!")
}
